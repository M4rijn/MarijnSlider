# MarijnSlider.

[Check out the demo here](http://school.marijnkleuskens.nl/fontys/jaar2/dev42/)

## Every slide can contain whatever markup you want.
It's recommended to give MarijnSlider a height, otherwise every slide with different content has a different height.

## Installation

#### Prerequisite
jQUery is needed to use MarijnSlider.
```
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
```

#### Link to MarijnSlider
```
<script src="path/to/marijnslider.js" type="text/javascript"></script>
<link href="path/to/marijnslider.css" rel="stylesheet" type="text/css" />
```

#### Call MarijnSlider
```
<script type="text/javascript">
    $(document).ready(function () {
        $(".marijn-slider").marijnSlider();
    });
</script>
```

## Use MarijnSlider
To use MarijnSlider you simply need to use this HTML structure
```
<div class="marijn-slider">
    <div class="marijn-slides">
        <ul>
            <li>
                <!-- Content -->
            </li>
            <li>
                <!-- Content -->
            </li>
            <li>
                <!-- Content -->
            </li>
            <!-- Etc -->
        </ul>
    </div>
</div> 
```

## Options
When calling MarijnSlider in your site's head you can use these options.
```
$(".marijn-slider").marijnSlider({
    autoSlide: false / 10s,
    arrowPosition: "outside" / "inside",
    arrowImage: "link/to/image.png",
    arrowSize: [w, h]
});
```