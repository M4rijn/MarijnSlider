(function($){

    $.fn.marijnSlider = function(options) {

        var settings = $.extend({
            // Defaults.
            autoSlide: false,
            arrowPosition: "outside",
            arrowImage: "js/marijnslider/images/arrow.svg",
            arrowSize: [35, 35]
        }, options);

        var marijnSlider = $(".marijn-slider");

        // Give all the sliders and slides the correct width, so that all the slides fit inside it.
        marijnSlider.each(function () {

            var $this = $(this);

            // How many slides are in the slider?
            var slides = $this.find("li").length;

            // Give the slider the correct width so that all the slides fit
            $this.find("ul").css({
                "width": 100 * slides + "%"
            });

            // Give every slide the correct percentage width to fit in the slider. Also give each slide the height of the slider (for scroll: auto;)
            $this.find("li").css({
                "width": 100 / slides + "%",
                "height": $this.height()
            });

            if (settings.arrowPosition){
                $this.append(
                    "<div class='arrows'>" +
                        "<div class='arrow arrow-left'><a style='background-image: url("+settings.arrowImage+"); width: "+ settings.arrowSize[0] +"px; height: "+ settings.arrowSize[1] +"px;'></a></div>" +
                        "<div class='arrow arrow-right'><a style='background-image: url("+settings.arrowImage+"); width: "+ settings.arrowSize[0] +"px; height: "+ settings.arrowSize[1] +"px;'></a></div>" +
                    "</div>"
                );
            }

            if(settings.arrowPosition === "inside"){
                $(".arrow").addClass("inside");
            }

        });

        $(".arrow-left").on("click", function () {
            scrollSlide("left");
        });

        $(".arrow-right").on("click", function () {
            scrollSlide("right");
        });

        $(document).on("keydown", function (event) {
            if (event.keyCode == 37) {
                scrollSlide("left");
                return false;
            } else if (event.keyCode == 39) {
                scrollSlide("right");
                return false;
            }
        });


        // Autoslide
        // Boolean so that the function can't be called while it is still running.
        var autoSlideBool = true;
        if(settings.autoSlide){
            autoslide();
            function autoslide() {
                console.log("autoSlideBool is " + autoSlideBool);
                if(autoSlideBool){
                    autoSlideBool = false;
                    setTimeout(function () {
                        autoSlideBool = true;
                        scrollSlide("right");
                    }, settings.autoSlide);
                }
            }
        }

        var sliderCounter = 0;

        function scrollSlide(direction) {

            // Calculate the width of the slides (so how much margin-left should be added or removed to the slider for it to show the next/previous slide)
            var slideWidth = marijnSlider.parent().width();

            //What direction are we going?
            if (direction === "left" && sliderCounter >= 1) {
                marijnSlider.find("ul").animate({
                    "margin-left": "+=" + slideWidth
                }, 500);
                sliderCounter--;

                // If autoslide is enabled call it again.
                if(settings.autoSlide){
                    autoslide();
                }

            } else if (direction === "right" && sliderCounter < marijnSlider.find("li").length - 1) {
                $(".marijn-slider").find("ul").animate({
                    "margin-left": "-=" + slideWidth
                }, 500);
                sliderCounter++;

                // If autoslide is enabled call it again.
                if(settings.autoSlide){
                    autoslide();
                }

            } else {
                console.log("Kan niet verder naar " + direction);
            }

            // Trigger the function that determines which arrows are visible
            arrowOpacity(sliderCounter);

        }

        // Determine which arrows are visible
        // Begin with the left arrow hidden
        arrowOpacity(0);
        function arrowOpacity(sliderCounter) {
            if ((marijnSlider.find("li").length - 1) === 0) {
                // There is only one slide.
                $(".arrow-left, .arrow-right").css({
                    "opacity": "0",
                    "transition": ".5s"
                });
            } else if (sliderCounter < 1) {
                // User is on the first slide
                $(".arrow-left").css({
                    "opacity": "0",
                    "transition": ".5s"
                });
                $(".arrow-right").css({
                    "opacity": "1",
                    "transition": ".5s"
                });
            } else if (sliderCounter >= 1 && sliderCounter < (marijnSlider.find("li").length - 1)) {
                // User is in one of the middle slides
                $(".arrow-left, .arrow-right").css({
                    "opacity": "1",
                    "transition": ".5s"
                });
            } else if (sliderCounter === (marijnSlider.find("li").length - 1)) {
                // User is on the last slide
                $(".arrow-left").css({
                    "opacity": "1",
                    "transition": ".5s"
                });
                $(".arrow-right").css({
                    "opacity": "0",
                    "transition": ".5s"
                });
            }
        }

    }

}(jQuery));